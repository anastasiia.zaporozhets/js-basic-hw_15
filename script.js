"use strict"

const btn = document.getElementById("btn");
const textDiv = document.querySelector('.text-div');

btn.addEventListener("click", () => {
    setTimeout(() => {
        textDiv.textContent = "Операція виконана успішно!!!"
        textDiv.style.color = "green"
    }, 3000)
})

//завдання два
document.addEventListener("DOMContentLoaded", function () {
    const timer = document.getElementById('timer');
    let count = 10;
    function countdownTimer() {
        if (count === 0) {
            timer.textContent = "Зворотній відлік завершено!";
            clearInterval(timerInterval);
        } else {
            timer.textContent = count;
            count--;
        }
    }

    countdownTimer();

    const timerInterval = setInterval(countdownTimer, 1000);
});